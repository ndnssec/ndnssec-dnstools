# Minimal image of NSD on an Alpine Linux image
FROM alpine:3.9

# Install required packages
RUN apk add --no-cache \
  nsd \
  # Used for zone signing, etc.
  ldns-tools

# For pid file
RUN mkdir -p /run/nsd
RUN chown nsd:nsd /run/nsd

# Create
# Start NSD with explicit default values
ENTRYPOINT ["nsd"]
CMD ["-d", "-c", "/etc/nsd/nsd.conf", "-f", "/var/db/nsd/nsd.db"]
