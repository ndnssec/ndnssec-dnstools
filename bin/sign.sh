#!/bin/sh
#
# This is a convenient program to generate KSK and ZSK for a given domain and
# to sign the respective zone files.
#
# usage: sign.sh -n ZONENAME -d ZONE_DEF_DIR\n [-a ALGORITHM]
# example:
#     sign.sh \
#       -n example.com \
#       -d ./conf/zones
#       -a RSASHA1-NSEC3-SHA1
#
#The instructions are adapted from the following Digital Ocean tutorials:
# https://web.archive.org/web/20190323140103/https://www.digitalocean.com/community/tutorials/how-to-set-up-dnssec-on-an-nsd-nameserver-on-ubuntu-14-04
#
# *Please follow Google's Shell Style Guide here!

# Check for dependencies
for cmd in ldns-keygen ldns-signzone; do
  if ! [ -x "$(command -v $cmd 2>/dev/null)" ]; then
    echo "'$cmd' is missing!";
    exit 1;
  fi
done


#######################################
# Prints usage instruction
# Arguments:
#   None
# Returns:
#   None
#######################################
print_usage() {
  printf 'usage: sign.sh -n ZONENAME -d ZONE_DEF_DIR\n [-a ALGORITHM]'
}

unset SIGN_N SIGN_D SIGN_A
SIGN_A='RSASHA1-NSEC3-SHA1'
ZSK_SIZE='1024'
KSK_SIZE='2048'
HASH_PROG='sha1sum'

if [[ $(uname -s) == 'Darwin' ]]; then
  HASH_PROG='shasum'
fi

while getopts 'n:d:a:h' flag; do
  case "${flag}" in
    # Zone name
    n) SIGN_N="${OPTARG}" ;;
    # Zone definition directory
    d) SIGN_D="${OPTARG}" ;;
    # Set algorithm
    a) SIGN_A="${OPTARG}" ;;
    # Show help
    h) print_usage; exit 0 ;;
  esac
done

# Check if the given zone file exist
ZONE_FILE="${SIGN_D}/${SIGN_N}.zone"
if ! [ -r "${ZONE_FILE}" ]; then
  echo "Cannot find zone file under ${ZONE_FILE}"
  exit 1;
fi

# Print usage if any of the parameters is empty
if [ -z "${SIGN_N}" ] || [ -z "${SIGN_D}" ] ; then
  print_usage;
  exit 1;
fi

# Generating keys
pushd "${SIGN_D}"

echo 'Trying to generate ZSK...'
ZSK=$(ldns-keygen -a ${SIGN_A} -b ${ZSK_SIZE} ${SIGN_N})
echo "\t Successfully created ZSK under:\n\t '$(pwd)/${ZSK}'"

echo 'Trying to generate KSK...'
KSK=$(ldns-keygen -k -a ${SIGN_A} -b ${KSK_SIZE} ${SIGN_N})
echo "\t Successfully created KSK under:\n\t '$(pwd)/${KSK}'"

echo "Trying to sing zone '${SIGN_N}'..."
SIGN_SALT=$(head -n 1000 /dev/random | ${HASH_PROG} | cut -b 1-16)
echo "\t ... using random salt '${SIGN_SALT}'"
if ldns-signzone -n -p -s "${SIGN_SALT}" "${SIGN_N}.zone" ${ZSK} ${KSK}; then
  echo "\t Successfully signed zone file under\n\t '${SIGN_N}.zone.signed'"
else
  echo '\t Failed to sign zone file!'
fi

popd
exit 0
